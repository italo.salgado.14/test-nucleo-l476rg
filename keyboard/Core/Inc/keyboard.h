/***************************************************
 * Nombre del modulo: keyboard.c
 *
 * Modulo creado para la asignatura Elo312
 * Laboratorio de Estructura de Computadores
 * del departamento de Electrónica de la Universidad
 * Técnica Federico Santa María. El uso o copia
 * esté permitido y se agracede mantener el nombre
 * de los creadores.
 *
 * Escrito inicialmente el 01/01/2004 Por Michael Kusch & Wolfgang Freund
 * Modificado el 24/09/2014           Por Mauricio Solís & Andrés Llico.
 * Modificado el 30/03/2020           Por Mauricio Solís.
 *
 * Descripción del módulo:
 * Definición del módulo driver teclado matricial
 * Contiene las funciones que permiten manejar el teclado matricial.
 ***************************************************/

#ifndef __KEYBOARD__H
#define __KEYBOARD__H

/*--->> Includes <<-----------------------------------------------------------*/
#include <stdint.h>
#include "gpio.h"

/*--->> Definition <<---------------------------------------------------------*/

/*--->> Data types <<---------------------------------------------------------*/

typedef struct
{
  GPIO_TypeDef *  tec0_port;
  uint16_t        tec0_pin;

  GPIO_TypeDef *  tec1_port;
  uint16_t        tec1_pin;

  GPIO_TypeDef *  tec2_port;
  uint16_t        tec2_pin;

  GPIO_TypeDef *  tec3_port;
  uint16_t        tec3_pin;

  GPIO_TypeDef *  tec4_port;
  uint16_t        tec4_pin;

  GPIO_TypeDef *  tec5_port;
  uint16_t        tec5_pin;

  GPIO_TypeDef *  tec6_port;
  uint16_t        tec6_pin;

  GPIO_TypeDef *  tec7_port;
  uint16_t        tec7_pin;

}keyboard_conf_t;






/*--->> API Definition <<-----------------------------------------------------*/
/**
* @brief          :Inicializa los puertos del
* driver del teclado.
* @retval         : void
**************************************************/
void keyboard_init(keyboard_conf_t* key_conf);


/**
 * @brief       : Lee la matriz de botones y
 * llama a la función keyboard_button_action() cuando
 * detecta que se ha presionado una tecla (o se ha mantenido
 * presionada una tecla).
 * @retval       : void
 * @note: Esta función debe ser llamada cada 10 [ms]
*/

void keyboard_tec_matrix(void);

#endif
