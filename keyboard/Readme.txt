KEYBOARD

________________________
|                      |
|  [1]  [2]  [3]  [A]  |
|                      |
|  [4]  [5]  [6]  [B]  |
|                      |
|  [7]  [8]  [9]  [C]  |
|                      |
|  [*]  [0]  [#]  [D]  |
|                      |
------------------------  
  ___________________
  |                 |
  | 0 1 2 3 4 5 6 7 |
  -------------------


Para este ejemplo se utilizó el puerto C del microcontrolador,
pero en teoría podría usarse cualquier puerto/pin.

En particular:
Los pines del teclado están Enumerados de izquierda a derecha como TEC0 .. TEC7

TEC0, TEC1, TEC2, TEC3 deben ir conectados mediante una R a VCC
Además estos pines deben ser conectdos a los correspondientes en el microcontrolador. 
(para este ejemplo se usaron Resistencias de 5k5).


TEC4, TEC5, TEC6, TEC7 deben ir conectados a los correspondientes del microntrolador.



