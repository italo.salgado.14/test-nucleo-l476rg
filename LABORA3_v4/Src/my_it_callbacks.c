/*
 * my_it_callbacks.c
 *
 *  Created on: 03-03-2020
 *      Author: isalgado
 */
//#include "main.h"
#include "gpio.h"
#include "tim.h"
#include "main.h"





//FUNCTIONS
void f1_port (void){

	//Reiniciamos al estado inicial, todos apagados
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, GPIO_PIN_RESET);

	//funcion 1, prendemos/apagamos todos los leds de a dos
	HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_6);//1
	HAL_Delay(50);
	HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_6);//3
	HAL_Delay(50);
	HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_9);//5
	HAL_Delay(50);
	HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_10);//7
	HAL_Delay(50);
	HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_4);//8
	HAL_Delay(50);
	HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_8);//6
	HAL_Delay(50);
	HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_7);//4
	HAL_Delay(50);
    HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_7);//2
    HAL_Delay(50);

}

void f2_port (void){

	//Reiniciamos al estado inicial, todos apagados
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, GPIO_PIN_RESET);

	//funcion 2, parpadean todos los leds
	HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_6);
	HAL_Delay(50);
	HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_6);

    HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_7);
    HAL_Delay(50);
    HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_7);

    HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_6);
    HAL_Delay(50);
    HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_6);

    HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_7);
    HAL_Delay(50);
    HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_7);

    HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_9);
    HAL_Delay(50);
    HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_9);

    HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_8);
    HAL_Delay(50);
    HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_8);

    HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_10);
    HAL_Delay(50);
    HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_10);

    HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_4);
    HAL_Delay(50);
    HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_4);

}


void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    if (htim->Instance == htim2.Instance)
    {
        /* Toggle LED */
    	HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_5);
    	call_action_to_LED();
    }
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
  if(GPIO_Pin == GPIO_PIN_13)
  {
    /* Toggle LED1 */
	  funcion_estupida();


  }
}

