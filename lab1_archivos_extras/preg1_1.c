#include <stdio.h>

int x;
unsigned int y;

void prt(int i)
{
    int j;
    for (j = 15; j >= 0; j--) (1<<j) & i ? putchar ('1') : putchar('0');
    putchar('\n');
}

void main(void)
{
    x = 15; prt(x);
    x = x << 2; prt(x);
    x = x >> 2; prt(x);
    y = 15; prt(y);
    y = y << 2; prt(y);
    y = y >> 2; prt(y);
    x =- 15; prt(x);
    x = x << 2; prt(x);
    x = x >> 2; prt(x);
    y =- 15; prt(y);
    y = y<<2; prt(y);
    y = y>>2; prt(y);
}
