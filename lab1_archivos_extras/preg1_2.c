#include <stdio.h>

int x;
unsigned int y;

char r;
unsigned char s;


void prt_16(int i)
{
    int j;
    for (j = 15; j >= 0; j--)
        (1<< j ) & i ? putchar('1') : putchar('0');
    putchar('\n');
}

void ptr_8(char i)
{
     signed char j;
     for (j = 7; j>=0; j--)
     	 (1<< j ) & i ? putchar('1') : putchar('0');
     putchar('\n');
}


void main(void)
{
    x = 0x7fff; prt_16(x); printf("x = %d \n",x);
    x++; prt_16(x); printf("x = %d \n",x);
    printf("======================================= \n");
    y = 0x7fff; prt_16(y); printf("y = %u \n",y);
    y++; prt_16(y); printf("y = %u \n",y);
    printf("======================================= \n");
    printf("x = %u \n",x);
    printf("======================================= \n");
    y = 0xffff; prt_16(y); printf("y = %u \n",y);
    y++; prt_16(y); printf("y = %u \n",y);
    printf("======================================= \n");
    r = 0xff; ptr_8(r); printf("r = %d \n", r);
    s = r; ptr_8(s); printf("s = %d \n", s);
}


