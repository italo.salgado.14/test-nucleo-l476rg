#include <stdio.h>

int *pia, *pib;
int a[5] = { 0, 1, 2, 3, 4};
int b[5] = { 62, 63, 64, 65, 66};
int j, x;

void prta(int x[],int n)
{
    int i;
    for (i = 0; i < n; i++) printf("%d ",x[i]);
    printf("\n\n");
}

void main(void)
{
    prta(a, 5); prta(b, 5);
    pia = a;
    x = 4;
    j = *(pia + x); printf("%d \n",j);
    pib = &a[0];
    j = *(pib + x); printf("%d \n",j);
    printf("=============================== \n");
    pia++; printf("%d \n",*pia);
    pib = pia + 1; printf("%d \n", *pib);
    *pia = *(pia + 3) + 8; printf("%d \n",*pia);
    *(pia + 1) = *(pia + 5) + 8; printf("%d \n",*(pia + 1));
    pib = b + 4; *pib = *(pia + 4);
    prta(a, 5); prta(b, 5);
    prta(&a[0],10);
}

