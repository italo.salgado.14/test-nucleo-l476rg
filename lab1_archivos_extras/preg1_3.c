#include <stdio.h>

int a[5] = { 0, 1, 2, 3, 4 };
int b[5] = { 62, 63, 64, 65, 66 };
int j, x;

void prta(int x[], int n)
{
    int i;
    for (i = 0; i < n; i++) printf("%d ", x[i]);
    printf("\n\n");
}

void main(void)
{
    prta(b, 5);
    for (j = 0; j < 5; j++)
        a[j] = 4 - j;

    prta(a, 5);

    a[1] = -45;
    a[j] = a[3];
    a[2] = a[x + 3] + 16;

    prta(a, 5);
    prta(b, 1);
    printf("%d \n", a[2]);
    prta(a, 12);
}

