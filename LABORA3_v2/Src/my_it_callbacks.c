/*
 * my_it_callbacks.c
 *
 *  Created on: 03-03-2020
 *      Author: isalgado
 */
#include "main.h"
#include "gpio.h"
#include "main.h"

void f1_port (void){


	HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_6);
	HAL_Delay(500);

    HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_7);
    HAL_Delay(500);

    HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_6);
    HAL_Delay(500);

    HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_7);
    HAL_Delay(500);

    HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_9);
    HAL_Delay(500);

    HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_8);
    HAL_Delay(500);

    HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_10);
    HAL_Delay(500);

    HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_4);
    HAL_Delay(500);

}

void f2_port (void){


	HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_6);
	HAL_Delay(100);
	HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_6);

    HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_7);
    HAL_Delay(100);
    HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_7);

    HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_6);
    HAL_Delay(100);
    HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_6);

    HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_7);
    HAL_Delay(100);
    HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_7);

    HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_9);
    HAL_Delay(100);
    HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_9);

    HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_8);
    HAL_Delay(100);
    HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_8);

    HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_10);
    HAL_Delay(100);
    HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_10);

    HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_4);
    HAL_Delay(100);
    HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_4);

}


void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
  if(GPIO_Pin == GPIO_PIN_13)
  {
    /* Toggle LED1 */
	  funcion_estupida();


  }
}

