/*
 * funciones.h
 *
 *  Created on: 08-10-2019
 *      Author: lailapc
 */

#ifndef FUNCIONES_H_
#define FUNCIONES_H_

unsigned int multiply_pow_2_v1 ( unsigned int number, unsigned int power );
unsigned int multiply_pow_2_v2 ( unsigned int number , unsigned int power );

#endif /* FUNCIONES_H_ */
