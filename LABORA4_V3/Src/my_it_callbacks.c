/*
 * my_it_callbacks.c
 *
 *  Created on: 07-03-2020
 *      Author: isalgado
 */
#include "main.h"
#include "adc.h"
#include "gpio.h"

uint16_t local_counter=0;
uint64_t final_value;
uint32_t value_adc1=0, value_adc2=0, value_adc3=0, value_adc4=0, value_adc5=0;

void prom_five_values_andshow(){
	final_value=value_adc1+value_adc2+value_adc3+value_adc4+value_adc5;
	final_value=final_value/5;

	//Valores proporcionales a el valor capturado en el promedio
	if (final_value > 500){ HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6,GPIO_PIN_SET); }
	if (final_value > 1000){ HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7,GPIO_PIN_SET); }
	if (final_value > 1500){ HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6,GPIO_PIN_SET); }
	if (final_value > 2000){ HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7,GPIO_PIN_SET); }
	if (final_value > 2500){ HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9,GPIO_PIN_SET); }
	if (final_value > 3000){ HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8,GPIO_PIN_SET); }
	if (final_value > 3500){ HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10,GPIO_PIN_SET); }
	if (final_value > 4000){ HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4,GPIO_PIN_SET); }

	if (final_value < 500){ HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6,GPIO_PIN_RESET); }
	if (final_value < 1000){ HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7,GPIO_PIN_RESET); }
	if (final_value < 1500){ HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6,GPIO_PIN_RESET); }
	if (final_value < 2000){ HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7,GPIO_PIN_RESET); }
	if (final_value < 2500){ HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9,GPIO_PIN_RESET); }
	if (final_value < 3000){ HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8,GPIO_PIN_RESET); }
	if (final_value < 3500){ HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10,GPIO_PIN_RESET); }
	if (final_value < 4000){ HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4,GPIO_PIN_RESET); }
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc){
	if(hadc->Instance == ADC1){
		//por cada llamada guardamos un valor distinto.
		switch(local_counter)
		{
		case 0:
			value_adc1 = HAL_ADC_GetValue(&hadc1);
			local_counter++;
			break;
		case 1:
			value_adc2 = HAL_ADC_GetValue(&hadc1);
			local_counter++;
			break;
		case 2:
			value_adc3 = HAL_ADC_GetValue(&hadc1);
			local_counter++;
			break;
		case 3:
			value_adc4 = HAL_ADC_GetValue(&hadc1);
			local_counter++;
			break;
		case 4:
			value_adc5 = HAL_ADC_GetValue(&hadc1);
			local_counter=0;
			break;
		}
	//por cada llamada encendemos proporcionalmente los LEDs
	prom_five_values_andshow();
	}
}
