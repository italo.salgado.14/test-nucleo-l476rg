# test-nucleo-l476rg

Repo with all examples used in Laboratory of uC, UTFSM. 2018.

More examples in nucleo-l476rg-master and Projects_example_of_CubeMX.

## Utils Links

INTERRUPCIÓN EXTERNA CON BOTON:
https://www.waveshare.com/wiki/STM32CubeMX_Tutorial_Series:_EXTI

HAL (pdf con todas las funciones HAL documentadas que existen):
https://www.st.com/content/ccc/resource/technical/document/user_manual/2f/71/ba/b8/75/54/47/cf/DM00105879.pdf/files/DM00105879.pdf/jcr:content/translations/en.DM00105879.pdf


TIMER:
http://www.emcu.eu/stm32-basic-timer/#PWM
http://www.emcu.eu/tim14-pwm-output-duty-cycle-regulated-using-a-potentiometer/

ADC:
http://elb105.com/stm32f4-configuracion-y-uso-basico-del-adc/
https://www.youtube.com/watch?v=VaAl9hnPGiA
https://www.youtube.com/watch?v=UWw2rY1N4sU&t=10s

SERVO CONTROLLER:
https://www.youtube.com/watch?v=WMS0t9WGqVw

ERROR IN SPRINTF:
https://community.st.com/s/question/0D50X00009XkYay/how-to-print-float-value-with-printf-in-truestudio-

ERROR DE SIGNO AL CASTEAR EN USART EL HAL_UART_TRANSMIT:
https://stackoverflow.com/questions/31730084/pointer-targets-in-passing-argument-differ-in-signedness

USART:
https://www.youtube.com/watch?v=vv4KB-TSJFU&t=302s
https://www.youtube.com/watch?v=cqeryMxc1vA

ERROR SI ES QUE SETEAN PINES QUE NO DEBEN SERTEAR (ERROR DE CONEXION (4)). NO SETEEN PINES 14 NI 13 QUE SE USAN PARA CARGAR LOS PROGRAMAS
Error in initializing ST-LINK device.
Reason: (4) No device found on target----------------------------------
https://www.youtube.com/watch?v=ClTZerhlozY&t=359s
https://www.youtube.com/watch?v=ixCauknRAHg
https://stackoverflow.com/questions/56186473/how-to-fix-unable-to-open-file-when-uploading-to-stm32-in-truestudio