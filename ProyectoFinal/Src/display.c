/***************************************************
* Nombre del modulo: display.c
*
* Modulo creado para la asignatura Elo312
* Laboratorio de Estructura de Computadores
* del departamento de Electrónica de la Universidad
* Técnica Federico Santa María. El uso o copia
* está permitido y se agracede mantener el nombre
* de los creadores.
*
* Escrito inicialmente el 01/01/2004 Por Michael Kusch & Wolfgang Freund
* Modificado el 19/09/2014           Por Mauricio Solís.
* Modificado el 28/09/2019			 Por Rodrigo Jimenez. (Adaptacion a STM32)
*
* Descripción del módulo:
* Modulo driver display
* Contiene las funciones que permiten manejar un display
* LCD.
***************************************************/

/*  Include section
*
***************************************************/
#include "display.h"


/*  Defines section
*
***************************************************/
#define LCD_PORT_INIT   {initControl();}


#define LSET_BIT(x, y)      (HAL_GPIO_WritePin(y, x, GPIO_PIN_SET))
#define LRST_BIT(x, y)      (HAL_GPIO_WritePin(y, x, GPIO_PIN_RESET))
#define LCD_WAIT()      udelay(120)

#define LCD_WRITE(x)    {_data_output(); _data_write(x); LCD_ENABLE(); }//data_input();}
//#define LCD_READ(x)     {LSET_BIT(LCD_E); LCD_PDELAY; (x) = data_read(); LRST_BIT(LCD_E); LCD_PDELAY;}
#define LCD_READ(x)     {LSET_BIT(LCD_E, E_GPIO_Port); HAL_Delay(1) ; (x) = _data_read(); LRST_BIT(LCD_E, E_GPIO_Port); HAL_Delay(1);}

#define LCD_PDELAY      {asm("NOP"); asm("NOP"); asm("NOP"); asm("NOP");}

#define LCD_ENABLE()    {LSET_BIT(LCD_E, E_GPIO_Port); HAL_Delay(1); LRST_BIT(LCD_E, E_GPIO_Port);}
#define LCD_WAIT()      udelay(120)

/*  Local Function Prototype Section
*
***************************************************/


void udelay(unsigned int);
unsigned char display_wait_BF(void);
char display_get_data(void);
unsigned char display_lcd_addr(int);
void display_send_cmd(unsigned char);
void display_send_data(unsigned char);
int putchar(int c);
int display_lcd_line(unsigned char);
int display_lcd_copy(unsigned char, unsigned char, int);
void display_lcd_scroll_up(void);

static unsigned char _data_read(void);
static void _data_write(unsigned char);
static void _data_input(void);
static void _data_output(void);
//static void _initControl(void);

/*  Global Variables Section
*
***************************************************/


////////////////////////////////////////
//        RUTINA A CALIBRAR           //
//                                    //
/**************************************************
* Nombre    		: void udelay(unsigned int arg1)
* returns			: void
* arg1				: Cantidad de microsegundos
* Creada por		: Michael Kusch
* Fecha creación		: 01/01/2004
* Descripción		: Hace un retardo de arg1
* microsegundos
**************************************************/
void udelay(unsigned int x)
{
	if(x/1000 < 1) HAL_Delay(1);
	else	HAL_Delay(x/1000);
}
//                                    //
////////////////////////////////////////


////////////////////////////////////////
//     RUTINAS NO IMPLEMENTADAS       //
//                                    //
/**************************************************
* Nombre    		: unsigned char display_wait_BF(void)
* returns			: Dirección actual del cursor
* Creada por		: Michael Kusch
* Fecha creación		: 01/01/2004
* Descripción		: Espera a que el display esté listo
* para recibir nuevos comandos o datos
**************************************************/
//#pragma inline = forced
unsigned char display_wait_BF(void)
{
	unsigned char busy;

	LSET_BIT(LCD_RW,RW_GPIO_Port);
	LRST_BIT(LCD_RS,RS_GPIO_Port);

	do
	{
		LCD_READ(busy);
	}
	while(busy & 0x80);

	LRST_BIT(LCD_RW,RW_GPIO_Port);
	return 0;
}


/**************************************************
* Nombre    		: void display_clear(void)
* returns			: void
* Creada por		: Mauricio Solís
* Fecha creación		: 19/09/2014
* Descripción		: Limpia el contenido del display
**************************************************/
void display_clear(void)
{
	display_send_cmd(0x01);
}


/**************************************************
* Nombre    		: void display_right_shift(void)
* returns			: void
* Creada por		: Mauricio Solís
* Fecha creación		: 19/09/2014
* Descripción		: Corre el "visor" del display en un
* caracter hacia la derecha
**************************************************/
void display_right_shift(void)
{
	display_send_cmd(0x1D);
}


/**************************************************
* Nombre    		: void display_left_shift(void)
* returns			: void
* Creada por		: Mauricio Solís
* Fecha creación		: 19/09/2014
* Descripción		: Corre el "visor" del display en un
* caracter hacia la izquierda
**************************************************/
void display_left_shift(void)
{
	display_send_cmd(0x18);
}

/**************************************************
* Nombre    		: void display_new_character(int arg1, char* arg2)
* returns			: void
* arg1			: Dirección del display donde se debe empezar a escribir
* arg2			: Puntero al arreglo que contiene el nuevo caracter a escribir
* Creada por		: Mauricio Solís
* Fecha creación		: 19/09/2014
* Descripción		: Escribe en el display un nuevo caracter arg2, en la dirección
* arg1
**************************************************/
void display_new_character(int pos, char *data)
{
	display_set_pos(pos);
	display_send_data(*data);
}

///////////////////////////////////////
//Rutinas Implementadas
///////////////////////////////////////

/**************************************************
* Nombre    		: unsigned char display_get_pos(void)
* returns			: Dirección actual del cursor
* Creada por		: Michael Kusch
* Fecha creación		: 01/01/2004
* Descripción		: Retorna la posición actual del cursor
**************************************************/
unsigned char display_get_pos(void)
{
	unsigned char val;
	display_wait_BF();
	LSET_BIT(LCD_RW,RW_GPIO_Port);
	LRST_BIT(LCD_RS,RS_GPIO_Port);
	LCD_READ(val);
	return val & 0x7F;
}

/**************************************************
* Nombre    		: char display_get_data(void)
* returns			: Caracter sobre el que está el cursor del display
* Creada por		: Michael Kusch
* Fecha creación		: 01/01/2004
* Descripción		: Envía el comando para leer el caracter sobre
* el que está el cursor del display y lo retorna
**************************************************/
char display_get_data(void)
{
	char value;
	display_wait_BF();
	LSET_BIT(LCD_RS,RS_GPIO_Port );
	LSET_BIT(LCD_RS,RS_GPIO_Port);
	LCD_READ(value);
	LRST_BIT(LCD_RS,RS_GPIO_Port);
	LRST_BIT(LCD_RW,RW_GPIO_Port);
	return value;
}
/**************************************************
* Nombre    		: void display_init(void)
* returns			: void
* Creada por		: Michael Kusch
* Fecha creación		: 01/01/2004
* Modificada por	:Rodrigo Jimenez
* Descripción		: Inicializa el display (modificado para stm32)
**************************************************/
void display_init(void)
{
	udelay(15000);
	_data_input();
	//LCD_PORT_INIT;
	LRST_BIT(LCD_RS, RS_GPIO_Port);
	LRST_BIT(LCD_RW, RW_GPIO_Port);

	LCD_WRITE(0x30);
	udelay(500);

	LCD_WRITE(0x30);
	udelay(120);

	LCD_WRITE(0x30);
	udelay(200);

	LCD_WRITE(0x38);
	udelay(200);
	LCD_WRITE(0x0F);

	udelay(200);
	LCD_WRITE(0x1);
	udelay(5000);
	LCD_WRITE(0x06);
	udelay(200);
}

/**************************************************
* Nombre    		: void display_set_pos(unsigned char arg1)
* returns			: void
* arg1			: Dirección a la que se debe enviar el cursor
* Creada por		: Michael Kusch
* Fecha creación		: 01/01/2004
* Descripción		: Setea la posición del cursor en el display
**************************************************/
void display_set_pos(unsigned char add)
{
	display_send_cmd(0x80 + add);
}


/**************************************************
* Nombre    		: unsigned char display_lcd_addr(int arg1)
* returns			: Dirección de la línea
* arg1			: Número de la línea
* Creada por		: Michael Kusch
* Fecha creación		: 01/01/2004
* Descripción		: Entrega la dirección de una determinada línea
* en el display.
* dado una línea (0, 1, etc) retorna la dirección de memoria
**************************************************/
unsigned char display_lcd_addr(int line)
{
	switch (LCD_LINES)
	{
		case 2:
			return (line > 0) ? 0x40 : 0x0;

		case 4:
			if (line == 0) return 0x0;
			else if (line == 1) return 0x40;
			else if (line == 2) return LCD_WIDTH;
			return (0x40 + LCD_WIDTH);

		default:
			return 0;
	}
}

/**************************************************
* Nombre    		: void display_send_cmd(unsigned char arg1)
* returns			: void
* arg1			: Comando a enviar
* Creada por		: Michael Kusch
* Fecha creación		: 01/01/2004
* Descripción		: Envía un comando al display
**************************************************/
void display_send_cmd(unsigned char value)
{
	display_wait_BF();
	LRST_BIT(LCD_RS, RS_GPIO_Port);
	LRST_BIT(LCD_RW, RW_GPIO_Port);
	LCD_WRITE(value);
}

/**************************************************
* Nombre    		: void display_send_data(char arg1)
* returns			: void
* arg1			: Data a enviar
* Creada por		: Michael Kusch
* Fecha creación		: 01/01/2004
* Descripción		: Envía un dato al display
**************************************************/
void display_send_data(unsigned char value)
{
	display_wait_BF();
	LSET_BIT(LCD_RS,RS_GPIO_Port); LRST_BIT(LCD_RW,RW_GPIO_Port);
	LCD_WRITE(value);
}

/**************************************************
* Nombre    		: int putchar(int arg1)
* returns			: El caracter ingresado como argumento
* arg1			: El caracter.
* Creada por		: Michael Kusch
* Fecha creación		: 01/01/2004
* Descripción		: Maneja los caracteres que "envía"
* la función printf de la biblioteca stdio
**************************************************/
int putchar(int c)
{
	if (c == '\n')
	{
		unsigned char pos = display_get_pos();

		if(pos >= display_lcd_addr(LCD_LINES - 1))
			display_lcd_scroll_up();

		display_set_pos(display_lcd_addr(LCD_LINES - 1));
		return c;
	}

	if (c == '\r')
	{
		unsigned char pos = display_get_pos();
		unsigned char line_start = display_lcd_addr(display_lcd_line(pos));

		display_set_pos(line_start);

		for(int i = 0; i < LCD_WIDTH; ++i)
		{
			display_send_data(' ');
		}

		display_set_pos(line_start);
		return c;
	}

	if (c == '\t')
	{
		display_send_data(' ');

	  	while(display_get_pos() % 4 != 0)
			display_send_data(' ');

		return c;
	}

	if (c == '\b')
	{
		unsigned char pos = display_get_pos();
		unsigned char prev_pos = pos;
		int line = display_lcd_line(pos);

		if(pos == display_lcd_addr(line))
		{
			if(line == 0) return c;
			prev_pos = display_lcd_addr(line - 1) + LCD_WIDTH - 1;
		}
		else
		{
			prev_pos = pos - 1;
		}

		display_set_pos(prev_pos);
		display_send_data(' ');
		display_set_pos(prev_pos);
		return c;
	}

	display_send_data((unsigned char) c);
	return c;
}

/**************************************************
* Nombre    		: int display_lcd_line(unsigned char addr)
* returns			: Número de la línea
* arg1			: Dirección del display
* Creada por		: Michael Kusch
* Fecha creación		: 01/01/2004
* Descripción		: Dada una dirección retorna el número de línea
**************************************************/
int display_lcd_line(unsigned char addr)
{
	int line;

	for (line = 0; line < LCD_LINES; line++)
		if (addr >= display_lcd_addr(line) &&
			addr < display_lcd_addr(line) + LCD_WIDTH)
			return line;

	return -1;
}

/**************************************************
* Nombre    		: void display_lcd_clear(int arg1, int arg2)
* returns			: void
* arg1			: Dirección de inicio
* arg2			: Cantidad de caracteres
* Creada por		: Michael Kusch
* Fecha creación		: 01/01/2004
* Descripción		: Limpia - arg2 - caracteres desde arg1
**************************************************/
void display_lcd_clear(int from, int len)
{
	int i;

	display_set_pos(from);
	for (i = 0; i < len; i++)
		display_send_data(' ');

	display_set_pos(from);
}

/**************************************************
* Nombre    		: int display_lcd_copy(unsigned char arg1, unsigned char arg2, int arg3)
* returns			: 0 si se hizo, -1 si hay un error de direcciones.
* arg1			: Dirección de inicio
* arg2			: Dirección destino
* arg3			: Cantidad de caracteres
* Creada por		: Michael Kusch
* Fecha creación		: 01/01/2004
* Descripción		: Copia - arg3 - caracteres desde - arg1 - a - arg2 -
**************************************************/
int display_lcd_copy(unsigned char from, unsigned char to, int len)
{
	char data[LCD_WIDTH];
	int i;

	if ((len > LCD_WIDTH) || (from > (0x7f - len))
		|| (to > (0x7f - len))) return -1;

	display_set_pos(from);
	for (i = 0; i < len; i++)
		data[i] = display_get_data();

	display_set_pos(to);
	for (i = 0; i < len; i++)
		display_send_data(data[i]);

	return 0;
}

/**************************************************
* Nombre    		: void display_lcd_scroll_up(void)
* returns			: void
* Creada por		: Michael Kusch
* Fecha creación		: 01/01/2004
* Descripción		: Copia línea i en línea i - 1 para todo el display
**************************************************/
void display_lcd_scroll_up(void)
{
	int line;

	for (line = 0; line < LCD_LINES - 1; line++)
		display_lcd_copy(display_lcd_addr(line + 1), display_lcd_addr(line), LCD_WIDTH);

	display_lcd_clear(display_lcd_addr(line), LCD_WIDTH);
}
//                                    //
////////////////////////////////////////


/**************************************************
* Nombre    		: void display_test_Write_CGRAM_MS(void)
* returns			: void
* Creada por		: Mauricio Solís
* Fecha creación		: 20/09/2014
* Descripción		: Escribe en la memoria CGRAM
* un caracter nuevo ... un robot.
**************************************************/

const char new_char0[]={0xA};
void display_test_Write_CGRAM_MS(void)
{
	display_register_character(0x1, new_char0);
}

void display_register_character(unsigned char idx, const char *bitmap)
{
	display_send_cmd(0x40 + (idx << 3));

	for(int i = 0; i < 8; ++i)
	{
		display_send_data(bitmap[i]);
	}
}


/**************************************************
* Nombre    		: void initControl(void)
* returns			: void
* Creada por		: Rodrigo Jimenez
* Fecha creación		: 28/09/2019
* Descripción		: Inicializa los puertos y pines para el display.
**************************************************/
/*
static void _initControl()
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	GPIO_InitStruct.Pin = LCD_E;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(E_GPIO_Port, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = LCD_RS;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(RS_GPIO_Port, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = LCD_RW;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(RW_GPIO_Port, &GPIO_InitStruct);

}
*/

/**************************************************
* Nombre    		: void data_output(void)
* returns			: void
* Creada por		: Rodrigo Jimenez
* Fecha creación		: 28/09/2019
* Descripción		: Establece los puertos y pines de datos respectivos como salidas.
**************************************************/
static void _data_output()
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	/*Configure GPIO pins : PAPin PAPin PAPin PAPin
	                           PAPin */
	  GPIO_InitStruct.Pin = D1_Pin|D0_Pin|D7_Pin|D2_Pin;
	  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	  GPIO_InitStruct.Pull = GPIO_NOPULL;
	  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	  /*Configure GPIO pins : PBPin PBPin PBPin PBPin
	                           PBPin */
	  GPIO_InitStruct.Pin = D6_Pin|D3_Pin|D5_Pin|D4_Pin;
	  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	  GPIO_InitStruct.Pull = GPIO_NOPULL;
	  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
}


/**************************************************
* Nombre    		: void data_input(void)
* returns			: void
* Creada por		: Rodrigo Jimenez
* Fecha creación		: 28/09/2019
* Descripción		: Establece los puertos y pines de datos respectivos como entradas.
**************************************************/
static void _data_input()
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	/*Configure GPIO pins : PAPin PAPin PAPin PAPin
	                           PAPin */
	  GPIO_InitStruct.Pin = D1_Pin|D0_Pin|D7_Pin|D2_Pin;
	  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	  /*Configure GPIO pins : PBPin PBPin PBPin PBPin
	                           PBPin */
	  GPIO_InitStruct.Pin = D6_Pin|D3_Pin|D5_Pin|D4_Pin;
	  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
}

static void _data_write(unsigned char x)
{
	HAL_GPIO_WritePin(D0_GPIO_Port,D0_Pin,(x & 0x01)!=0);
	HAL_GPIO_WritePin(D1_GPIO_Port,D1_Pin,(x & 0x02)!=0);
	HAL_GPIO_WritePin(D2_GPIO_Port,D2_Pin,(x & 0x04)!=0);
	HAL_GPIO_WritePin(D3_GPIO_Port,D3_Pin,(x & 0x08)!=0);
	HAL_GPIO_WritePin(D4_GPIO_Port,D4_Pin,(x & 0x10)!=0);
	HAL_GPIO_WritePin(D5_GPIO_Port,D5_Pin,(x & 0x20)!=0);
	HAL_GPIO_WritePin(D6_GPIO_Port,D6_Pin,(x & 0x40)!=0);
	HAL_GPIO_WritePin(D7_GPIO_Port,D7_Pin,(x & 0x80)!=0);
}

static unsigned char _data_read()
{
	_data_input();
	unsigned char x;
	GPIO_PinState bits[8];
	bits[0] = HAL_GPIO_ReadPin(D0_GPIO_Port,D0_Pin);
	bits[1] = HAL_GPIO_ReadPin(D1_GPIO_Port,D1_Pin);
	bits[2] = HAL_GPIO_ReadPin(D2_GPIO_Port,D2_Pin);
	bits[3] = HAL_GPIO_ReadPin(D3_GPIO_Port,D3_Pin);
	bits[4] = HAL_GPIO_ReadPin(D4_GPIO_Port,D4_Pin);
	bits[5] = HAL_GPIO_ReadPin(D5_GPIO_Port,D5_Pin);
	bits[6] = HAL_GPIO_ReadPin(D6_GPIO_Port,D6_Pin);
	bits[7] = HAL_GPIO_ReadPin(D7_GPIO_Port,D7_Pin);
	x = bits[0]*1+bits[1]*2+bits[2]*4+bits[3]*8+bits[4]*16+bits[5]*32+bits[6]*64+bits[7]*128;
	return x;

}

