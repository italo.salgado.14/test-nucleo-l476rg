/*
 * callbacks.c
 *
 *  Created on: 29-03-2020
 *      Author: laila
 */

/* Includes */
#include "main.h"
#include "adc.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"
#include "servo.h"

/* Functions */
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc){
	adc_value = HAL_ADC_GetValue(hadc); //leer el valor adc
	resize_adc_lecture(&adc_value);
	val1=new_value_to_led(adc_value,3);
	val2=new_value_to_led(adc_value,7);
	to_servo=value_to_servo(adc_value, &to_servo1, &to_servo2, &to_servo3, &counter);


	//to servo
	__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_1, adc_value);

	//to LEDs
	__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, val1);
	__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2, val1);

	//reiniciar la conversion adc porque CONTINUOUS CONVERSION MODE
	//en el cubo no esta en "enable"
	HAL_ADC_Start_IT(&hadc1);
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart){
	if (huart->Instance == huart2.Instance){
		HAL_UART_Transmit(&huart2, (uint8_t*) &msg, 20, 100);

		if (buffer_recibe[0] == 97){

		}
		if (buffer_recibe[0] == 101){

		}
		HAL_UART_Receive_IT(&huart2, buffer_recibe, sizeof(buffer_recibe));
	}
}

uint8_t counter;
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{

    if (htim->Instance == htim3.Instance)
    {
        /* Toggle LED */

    }
}

