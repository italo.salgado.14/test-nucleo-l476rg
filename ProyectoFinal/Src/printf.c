
#include "printf.h"

#include <stdarg.h>
#include "mini-printf.h"

char buffer[512];

void printf(const char* format, ...)
{
	va_list va;

	va_start(va, format);
	snprintf(buffer, sizeof(buffer), format, va);
	va_end(va);

	char *c = buffer;
	while(*c) putchar(*c++);
}
