/*
 * servo.c
 *
 *  Created on: 29-03-2020
 *      Author: laila
 */
/* Includes */
#include <stdio.h>

/* Functions*/

/*
* funcion de desplazamiento de 10 niveles. Si el desplazamiento
* sobrepasa al máximo valor definido para el adc, reinicia el numero
* para dar siempre entero. Como resize_adc_lecture = 2048 -> cada desp= 2048/10 = 204
* aplicar rezice del valor adc con anterioridad
*/
uint16_t new_value_to_led(uint16_t num,uint16_t desp){
	uint16_t value=0;
	if(num+desp>2048){
		value = num + 204*desp - 2048;
		return value;
	}
	else{
		value=num + 204*desp;
		return value;
	}
}

/*
* usa simple funcion para acercar el valor que lee el adc
* al valor maximo que puede tomar el timer
* como el adc se encuentra entre 0 y 4096, esta funcion
* le deja entre 0 y 2048 para ocupar el potenciometro completo
*/
void resize_adc_lecture(uint16_t* num ){
	*num = *num/2;
}

/*
 * esta funcion acondiciona el valor del potenciometro, logrando que
 * todo el potenciometro logre adecuarse a el movimiento de el servo
 * además, calcula un promedio de los ultimos 3 valores e ignora las
 * lecturas provenientes del ruido
*/
uint16_t value_to_servo(uint16_t num, uint16_t* measure1, uint16_t* measure2,
		uint16_t* measure3, uint8_t* counter){
	if (*measure1 == 0){
		*measure1 = 78 + 0.08300781*num;
		*measure2 = 78 + 0.08300781*num;
		*measure3 = 78 + 0.08300781*num;
		return *measure1;
	}
	else {
		int dif = (int)(*measure1 + *measure2 + *measure3)/3;
		dif = dif - num*0.08300781- 78;


		//tolerancia de lectura: reduccion de ruido
		if ( dif > 5 || dif < -5 ){
			switch (*counter){
				case 0:
					*measure1 = 78 + 0.08300781*num;
					*counter = *counter + 1;
					return (*measure1 + *measure2 + *measure3)/3;
				case 1:
					*measure2 = 78 + 0.08300781*num;
					*counter = *counter + 1;
					return (*measure1 + *measure2 + *measure3)/3;
				case 2:
					*measure3 = 78 + 0.08300781*num;
					*counter = 0;
					return (*measure1 + *measure2 + *measure3)/3;
			}
		}
		else {
			return (*measure1 + *measure2 + *measure3)/3;
		}

	}
	return 0;
}
