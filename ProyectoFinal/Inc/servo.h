/*
 * servo.h
 *
 *  Created on: 29-03-2020
 *      Author: laila
 */

#ifndef SERVO_H_
#define SERVO_H_

uint16_t new_value_to_led(uint16_t,uint16_t);
void resize_adc_lecture(uint16_t*);
uint16_t condition_to_servo(uint16_t);
uint16_t value_to_servo(uint16_t, uint16_t*, uint16_t*, uint16_t* , uint8_t*);

#endif /* SERVO_H_ */
