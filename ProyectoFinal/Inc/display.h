#ifndef DISPLAY__H
#define DISPLAY__H

//#include "msp430_version.h"
#include "main.h"

#ifdef MSP430_GCC
	#include "printf.h"
#else
	//int putchar(int c);
#endif


// defines display layout
#define LCD_LINES			2
#define LCD_WIDTH			16

// defines for control

#define LCD_RS	RS_Pin	// bit meaning: 1 = Data , 0 = Control
#define LCD_RW	RW_Pin	// bit meanings: 0 = Write, 1 = Read
#define LCD_E	E_Pin	// Enable

void udelay(unsigned int x);
void display_clear(void);
void display_right_shift(void);
void display_left_shift(void);
void display_new_character(int, char*);
unsigned char display_get_pos(void);
void display_init(void);
void display_set_pos(unsigned char);
void display_lcd_clear(int, int);
unsigned char display_lcd_addr(int line);
void display_test_Write_CGRAM_MS(void);
void display_register_character(unsigned char addr, const char *bitmap);

#endif
