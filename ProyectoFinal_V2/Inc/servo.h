/*
 * servo.h
 *
 *  Created on: 29-03-2020
 *      Author: laila
 */

#ifndef SERVO_H_
#define SERVO_H_

/*
* @brief funcion de desplazamiento de 10 niveles. Si el desplazamiento
*        sobrepasa al máximo valor definido para el adc, reinicia el numero
*        para dar siempre entero. Como resize_adc_lecture = 2048 -> cada desp= 2048/10 = 204
*        aplicar rezice del valor adc con anterioridad
* @param numero
* @param desplazamiento
* @retval None
*/
uint16_t new_value_to_led(uint16_t,uint16_t);

/*
* @brief usa simple funcion para acercar el valor que lee el adc
*        al valor maximo que puede tomar el timer
*        como el adc se encuentra entre 0 y 4096, esta funcion
*        le deja entre 0 y 2048 para ocupar el potenciometro completo.
*        Modifica directamente la variable
* @param lectura adc
* @retval None
*/
void resize_adc_lecture(uint16_t*);


/*
 * @brief esta funcion acondiciona el valor del potenciometro, logrando que
 *        todo el potenciometro logre adecuarse a el movimiento de el servo
 *        además, calcula un promedio de los ultimos 3 valores e ignora las
 *        lecturas provenientes del ruido
 * @param Valor Leido
 * @param Valor 1 guardado
 * @param Valor 2 guardado
 * @param Valor 3 guardado
 * @param Contador interno, externo si es que es necesario.
 * @retval None
 *
*/
uint16_t value_to_servo(uint16_t, uint16_t*, uint16_t*, uint16_t* , uint8_t*);

/*
 * @brief Control de luminosidad de los 8 leds
 *        que indican el movimiento del servo actual
 * @param lectura de valor del servo
 * @retval None
 */
void servo_to_leds(int);

/*
 * @brief Control de luminosidad de el led Rojo-Verde
 * @param contador del led
 * @brief Valor a mandar al led
 */
uint16_t led_lum(uint16_t counter);

#endif /* SERVO_H_ */
