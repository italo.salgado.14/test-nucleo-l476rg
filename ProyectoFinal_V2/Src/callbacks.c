/*
 * callbacks.c
 *
 *  Created on: 29-03-2020
 *      Author: laila
 */

/* Includes */
#include "main.h"
#include "adc.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"
#include "servo.h"
#include <stdio.h>
enum {REMOTE_CTRL_ON, REMOTE_CTRL_OFF};


/* Functions Callbacks */

/*
 * En esta funcion se espera:
 * 1) Leer valor adc
 * 2) Cambiar la escala del adc
 * 3) Si y solo si el modo remoto esta en modo activo, podemos manejar el servo.
 *  De otra manera, estará bloqueado en la posición bloqueada.
 */
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc){
  freq_led = freq_led + 1;

  if(remote_mode == REMOTE_CTRL_ON){
    adc_value = HAL_ADC_GetValue(hadc); //leer el valor adc
    resize_adc_lecture(&adc_value); //resize para valor
    to_servo=value_to_servo(adc_value, &to_servo1, &to_servo2, &to_servo3, &counter);


    //to servo + LEDs servo
    __HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_1, to_servo);
    servo_to_leds(to_servo);

    //to LEDs
    __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, 1000);
    __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2, 0);

    HAL_ADC_Start_IT(&hadc1);//reiniciar la conversion adc porque CONTINUOUS CONVERSION MODE en el cubo no esta en "enable"
  }
  else {

    //to_servo + LEDs
    __HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_1, 80);
    servo_to_leds(80);

    //to LEDs
    __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, 0);
    __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2, 1000);

  }
}

/*
 * Reconocemos la entrada desde la computadora. Una "e" habilita los controles
 * locales. Una "a" deshabilita los controles locales.
 */

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart){
  HAL_UART_Transmit(&huart2, (uint8_t*) &msg, 20, 100);
	if (huart->Instance == huart2.Instance){
		if (buffer_recibe[0] == 97){ //a (apagado)
		  remote_mode = REMOTE_CTRL_OFF;
		}
		if (buffer_recibe[0] == 101){ //e (encendido)
		  remote_mode = REMOTE_CTRL_ON;
		}
		HAL_UART_Receive_IT(&huart2, buffer_recibe, sizeof(buffer_recibe));
	}
}

/*
  Callback timer (no necesario por ahora)


void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  if (htim->Instance == htim3.Instance)
  {

  }
}

*/
