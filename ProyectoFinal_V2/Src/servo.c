/*
 * servo.c
 *
 *  Created on: 29-03-2020
 *      Author: laila
 */
/* Includes */
#include <stdio.h>
#include "gpio.h"
/* Functions*/


uint16_t new_value_to_led(uint16_t num,uint16_t desp){
	uint16_t value=0;
	if(num+desp>2048){
		value = num + 204*desp - 2048;
		return value;
	}
	else{
		value=num + 204*desp;
		return value;
	}
}


void resize_adc_lecture(uint16_t* num ){
	*num = *num/2;
}


uint16_t value_to_servo(uint16_t num, uint16_t* measure1, uint16_t* measure2,
		uint16_t* measure3, uint8_t* counter){
	if (*measure1 == 0){
		*measure1 = 73 + 0.0777*num;
		*measure2 = 73 + 0.0777*num;
		*measure3 = 73 + 0.0777*num;
		return *measure1;
	}
	else {
		int dif = (int)(*measure1 + *measure2 + *measure3)/3;
		dif = dif - num*0.08300781- 78;


		//tolerancia de lectura: reduccion de ruido
		if ( dif > 5 || dif < -5 ){
			switch (*counter){
				case 0:
					*measure1 = 73 + 0.0777*num;
					*counter = *counter + 1;
					return (*measure1 + *measure2 + *measure3)/3;
				case 1:
					*measure2 = 73 + 0.0777*num;
					*counter = *counter + 1;
					return (*measure1 + *measure2 + *measure3)/3;
				case 2:
					*measure3 = 73 + 0.0777*num;
					*counter = 0;
					return (*measure1 + *measure2 + *measure3)/3;
			}
		}
		else {
			return (*measure1 + *measure2 + *measure3)/3;
		}

	}
	return 0;
}


void servo_to_leds(int final_value){
  //Valores proporcionales a el valor capturado en el promedio
  if (final_value > 75){ HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10,GPIO_PIN_SET); }
  if (final_value > 100){ HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5,GPIO_PIN_SET); }
  if (final_value > 125){ HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10,GPIO_PIN_SET); }
  if (final_value > 150){ HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8,GPIO_PIN_SET); }
  if (final_value > 175){ HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9,GPIO_PIN_SET); }
  if (final_value > 200){ HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7,GPIO_PIN_SET); }
  if (final_value > 225){ HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6,GPIO_PIN_SET); }
  if (final_value > 250){ HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5,GPIO_PIN_SET); }

  if (final_value < 75){ HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10,GPIO_PIN_RESET); }
  if (final_value < 100){ HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5,GPIO_PIN_RESET); }
  if (final_value < 125){ HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10,GPIO_PIN_RESET); }
  if (final_value < 150){ HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8,GPIO_PIN_RESET); }
  if (final_value < 175){ HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9,GPIO_PIN_RESET); }
  if (final_value < 200){ HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7,GPIO_PIN_RESET); }
  if (final_value < 225){ HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6,GPIO_PIN_RESET); }
  if (final_value < 250){ HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5,GPIO_PIN_RESET); }
}

#define MAXCOUNT 60000

uint16_t led_lum(uint16_t counter){
  if (counter < MAXCOUNT/2){
    return counter;
  }
  if (counter >= MAXCOUNT/2){
    return (MAXCOUNT - counter);
  }
  return 0;
}

